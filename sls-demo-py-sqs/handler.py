import json
import os
import boto3

ssm = boto3.client('ssm')
lmbda = boto3.client('lambda')

def hello(event, context):
    
    lpl_environ = os.environ['LPL_ENVIRONMENT']
    sqs_queue_arn = ssm.get_parameter(Name=f"/{lpl_environ}/advisor_review/sqs_queue_arn", WithDecryption=True)['Parameter']['Value']
    sqs_queue_uri = ssm.get_parameter(Name=f"/{lpl_environ}/advisor_review/sqs_queue_uri", WithDecryption=True)['Parameter']['Value']
    random_lambda_arn = ssm.get_parameter(Name=f"/{lpl_environ}/advisor_review/random_lambda_arn", WithDecryption=True)['Parameter']['Value']
    
    
    print("Hello from Lambda!")
    print(f"Running in: {lpl_environ}")
    print(f"Connected to SQS Queue {sqs_queue_arn} at {sqs_queue_uri}")
    print(f"Invoking Lambda random number generator {random_lambda_arn}")
    
    output = lmbda.invoke(FunctionName=random_lambda_arn, InvocationType='RequestResponse')
    
    randomNumber = output['Payload'].read().decode("utf-8")
    
    print(f"Random number is {randomNumber}")
    
    try:
        for record in event['Records']:
            body = record['body']
            print(f"The message from the queue is: {body}")
    except Exception as e:
        # Send some context about this error to Lambda Logs
        print(e)
        # throw exception, do not handle. Lambda will make message visible again.
        raise e

